﻿using Beverages.Domain.Abstractions;

namespace Beverages.Application.Interfaces.Repositories
{
    /// <summary>
    /// Describes the abstraction of the implementation of the IUnitOfWork pattern
    /// </summary>
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : Entity;

        #region "SaveChanges"

        int SaveChanges();

        Task<int> SaveChangesAsync();

        #endregion
    }
}
