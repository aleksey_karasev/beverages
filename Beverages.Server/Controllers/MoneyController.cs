﻿using Beverages.Application.DTOs.Coins;
using Beverages.Application.Interfaces.Services;
using Beverages.Shared;
using Microsoft.AspNetCore.Mvc;

namespace Beverages.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MoneyController : Controller
    {
        private readonly IMoneyService _moneyService;

        public MoneyController(IMoneyService moneyService)
        {
            _moneyService = moneyService;
        }

        [HttpGet("getMoney")]
        public async Task<Result<List<CoinDTOResponse>>> GetChange()
        {
            return await _moneyService.GetChange();
        }

        [HttpPost("insertMoney")]
        public async Task<Result<decimal>> InsertMoney(CoinDTOReqest coin)
        {
            return await _moneyService.InsertMoney(coin);
        }

        [HttpGet("showAmount")]
        public async Task<Result<decimal>> ShowAmount()
        {
            return await _moneyService.ShowAmount();
        }
    }
}
