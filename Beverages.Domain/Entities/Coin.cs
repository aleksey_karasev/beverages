﻿
using Beverages.Domain.Abstractions;

namespace Beverages.Domain.Entities
{
    public class Coin : Entity
    {
        public decimal? Denomination { get; set; }
    }
}
