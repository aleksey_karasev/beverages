
import React, { useEffect, useState } from 'react';
import DrinkModal from './DrinkModal';
import Drink from './Props';
import './AdminInterfacePage.css';
import CoinsControl from '../coins/CoinsControl';

interface AdminInterfaceProps {
    coinsEnable: { [key: number]: boolean };
    toggleCoinEnable: (coinValue: number) => void;
}


const AdminInterfacePage: React.FC<AdminInterfaceProps> = ({ coinsEnable, toggleCoinEnable }) => {
    const [showCreateForm, setShowCreateForm] = useState(false);
    const [showEditForm, setShowEditForm] = useState(false);
    const [showDeleteForm, setShowDeleteForm] = useState(false);

    const [selectedDrinkId, setSelectedDrinkId] = useState<number | null>(null);

    const [drinks, setDrinks] = useState<Drink[]>([]);

    const [notification, setNotification] = useState<string>('');

    const [secretKey, setSecretKey] = useState<string>('');

    useEffect(() => {
        fetch(`https://localhost:7168/Drink/drinks`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then(data => {
                setDrinks(data.data);
            })
            .catch(error => console.log(`Error deleting drink:`, error));

        setFadeIn(true);
    }, []);

    const clickCreateDrink = () => {

        setSelectedDrinkId(null);
        setShowCreateForm(true);
    };

    const handleCreateDrink = (drink: Drink) => {

        var drinkDTO = {
            name: drink.name,
            count: drink.count,
            price: drink.price,
            imageName: drink.imageName
        }

        fetch(`https://localhost:7168/Drink/drinks?secretKey=${secretKey}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(drinkDTO)
        })
            .then(response => {

                return response.json();
            })
            .then(data => {
                setNotification(data.message); 
            })
            .then(() => {
                setTimeout(() => {

                    setNotification('');
                    window.location.reload();
                }, 2500);
            })
            .catch(error => {
                console.error('Error creating drink:', error);
                setNotification(error.message);
            });

    };

    const handleUpdateDrink = (drink: Drink) => {

        var drinkDTO = {
            name: drink.name,
            count: drink.count,
            price: drink.price,
            imageName: drink.imageName
        }

        fetch(`https://localhost:7168/Drink/drinks/${selectedDrinkId}?secretKey=${secretKey}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(drinkDTO)
        })
            .then(response => {

                return response.json();
            })
            .then(data => {
                setNotification(data.message);
            })
            .then(() => {
                setTimeout(() => {

                    setNotification('');
                    window.location.reload();
                }, 2500);
            })
            .catch(error => {
                console.error('Error creating drink:', error);
                setNotification(error.message);
            });
    };

    const handleDeleteDrink = () => {

        fetch(`https://localhost:7168/Drink/drinks/${selectedDrinkId}?secretKey=${secretKey}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {

                return response.json();
            })
            .then(data => {
                setNotification(data.message);
            })
            .then(() => {
                setTimeout(() => {

                    setNotification('');
                    window.location.reload();
                }, 2500);
            })
            .catch(error => {
                console.error('Error creating drink:', error);
                setNotification(error.message);
            });
    };


    const handleSelectDrink = (id: number) => {
        setSelectedDrinkId(id);
    };

    const [fadeIn, setFadeIn] = useState(false);

    return (
        <div className={fadeIn ? "fadeIn" : ""}>

            <div>
                <input
                    type="text"
                    value={secretKey}
                    onChange={(e) => setSecretKey(e.target.value)}
                    placeholder="Secret key"
                    style={{ width: '200px' }}
                />
            </div>

            <h2>Admin Interface</h2>

            <h3>Drinks Inventory</h3>
            <button onClick={() => clickCreateDrink()}>Create</button>
            <button onClick={() => setShowEditForm(true)}>Edit</button>
            <button onClick={() => setShowDeleteForm(true)}>Delete</button>

            <br />
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Count</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {drinks.map((drink) => (
                        <tr key={drink.id} className={selectedDrinkId === drink.id ? 'selected' : ''} onClick={() => handleSelectDrink(drink.id ?? 0)}>
                            <td>{drink.id}</td>
                            <td>{drink.name}</td>
                            <td>{drink.price} rub</td>
                            <td>{drink.count}</td>
                            <td>
                                <img src={`../src/assets/${drink.imageName}`} alt={drink.name} style={{ width: '50px', height: '50px' }} />
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {notification && (
                <p style={{ backgroundColor: 'lightblue', padding: '10px', borderRadius: '5px' }}>
                    {notification}
                </p>
            )}

            <DrinkModal
                title={"Create"}
                isOpen={showCreateForm}
                disable={false}
                onClose={() => setShowCreateForm(false)}
                onSave={handleCreateDrink}
            />
            <DrinkModal
                title={"Edit"}
                isOpen={showEditForm}
                disable={false}
                onClose={() => setShowEditForm(false)}
                onSave={handleUpdateDrink}
            />
            <DrinkModal
                title={"Delete"}
                isOpen={showDeleteForm}
                disable={true}
                onClose={() => setShowDeleteForm(false)}
                onSave={handleDeleteDrink}
            />

            <CoinsControl toggleCoinEnable={toggleCoinEnable} coinsEnable={coinsEnable} />
        </div>
    );
};

export default AdminInterfacePage;