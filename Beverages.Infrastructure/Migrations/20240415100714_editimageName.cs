﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Beverages.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class editimageName : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhotoURL",
                table: "Drinks",
                newName: "ImageName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImageName",
                table: "Drinks",
                newName: "PhotoURL");
        }
    }
}
