﻿
using Beverages.Application.DTOs.Drinks;
using Beverages.Application.Interfaces.Services;
using Beverages.Server.Filters;
using Beverages.Shared;
using Microsoft.AspNetCore.Mvc;

namespace Beverages.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DrinkController : Controller
    {
        private readonly IDrinkService _drinkService;

        public DrinkController(IDrinkService drinkService)
        {
            _drinkService = drinkService;
        }

        [SecretKeyFilter]
        [HttpPost("drinks")]
        public async Task<ActionResult<Result<Guid>>> Create(DrinkDTORequest drinkDTO)
        {
            var res = await _drinkService.Create(drinkDTO);
            return res.Success ? Ok(res) : BadRequest(res);
        }

        [SecretKeyFilter]
        [HttpPut("drinks/{id}")]
        public async Task<Result<bool>> Update(Guid id, DrinkDTORequest drinkDTO)
        {
            return await _drinkService.Update(id, drinkDTO);
        }

        [SecretKeyFilter]
        [HttpDelete("drinks/{id}")]
        public async Task<Result<bool>> Delete(Guid id)
        {
            return await _drinkService.Delete(id);
        }

        [HttpGet("drinks")]
        public async Task<Result<List<DrinkDTOResponse>>> GetAllDrinks()
        {
            return await _drinkService.GetAll();
        }

        [HttpGet("drinks/{id}")]
        public async Task<Result<DrinkDTOResponse>> GetDrinkById(Guid id)
        {
            return await _drinkService.GetById(id);
        }

        [HttpGet("drinks/count/{id}")]
        public async Task<Result<uint>> GetCountDrink(Guid id)
        {
            return await _drinkService.GetCountDrink(id);
        }

        [HttpGet("drinks/purchase/{id}")]
        public async Task<Result<decimal>> Purchase(Guid id)
        {
            return await _drinkService.Purchase(id);
        }
    }
}
