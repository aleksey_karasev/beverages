﻿using Beverages.Domain.Entities;
using Beverages.Persistence.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Beverages.Persistence.Seeds
{
    public class ApplicationDbInitializer
    {
        public async static void Seed(IApplicationBuilder app)
        {
            using(var scope = app.ApplicationServices.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

                context?.Database.EnsureCreated();

                if (!context.Drinks.Any())
                {
                    context.Drinks.AddRange(
                        new Drink { Name = "Cola", Price = 50m, Count = 50, ImageName = "cola.jpg", CreateDate = DateTime.UtcNow },
                        new Drink { Name = "Water", Price = 25m, Count = 100, ImageName = "water.jpg", CreateDate = DateTime.UtcNow },
                        new Drink { Name = "Tea", Price = 60m, Count = 75, ImageName = "icetea.png", CreateDate = DateTime.UtcNow }
                    );

                    await context.SaveChangesAsync();
                }

                if (!context.Coins.Any())
                {
                    var coin1Id = Guid.NewGuid();
                    var coin2Id = Guid.NewGuid();
                    var coin5Id = Guid.NewGuid();
                    var coin10Id = Guid.NewGuid();

                    context.Coins.AddRange(
                        new Coin { Id = coin1Id, Denomination = 1, CreateDate = DateTime.UtcNow },
                        new Coin { Id = coin2Id, Denomination = 2, CreateDate = DateTime.UtcNow },
                        new Coin { Id = coin5Id, Denomination = 5, CreateDate = DateTime.UtcNow },
                        new Coin { Id = coin10Id, Denomination = 10, CreateDate = DateTime.UtcNow }
                    );

                    context.StorageCoins.AddRange(
                        new StorageCoin { CoinId = coin1Id, Count = 100, CreateDate = DateTime.UtcNow },
                        new StorageCoin { CoinId = coin2Id, Count = 100, CreateDate = DateTime.UtcNow },
                        new StorageCoin { CoinId = coin5Id, Count = 100, CreateDate = DateTime.UtcNow },
                        new StorageCoin { CoinId = coin10Id, Count = 100, CreateDate = DateTime.UtcNow }
                    );

                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
