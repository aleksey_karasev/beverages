﻿
using Beverages.Application.Common.Mappings;
using Beverages.Domain.Entities;

namespace Beverages.Application.DTOs.Drinks
{
    public class DrinkDTORequest : ICreateMap<Drink>
    {
        public string? Name { get; set; }
        public string? ImageName { get; set; }
        public decimal? Price { get; set; }
        public uint? Count { get; set; }
    }
}
