
using Beverages.Persistence.Extensions;
using Beverages.Application.Extensions;
using Beverages.Persistence.Seeds;

namespace Beverages.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();

            builder.Services.AddApplicationServices();
            builder.Services.AddPersistenceServices(builder.Configuration);

            var app = builder.Build();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseAppExtensions();

            app.MapFallbackToFile("/index.html");

            ApplicationDbInitializer.Seed(app);

            app.Run();

        }
    }
}
