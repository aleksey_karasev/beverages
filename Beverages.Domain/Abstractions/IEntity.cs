﻿
namespace Beverages.Domain.Abstractions
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTime? CreateDate { get; set; }
        DateTime? EditDate { get; set; }
    }
}
