import './App.css';
import AdminInterfacePage from './components/adminInterface/AdminInterfacePage';
import UserInterfacePage from './components/userInterface/UserInterfacePage';

import { useState } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';

function App() {
    const [coinsEnable, setCoinsEnable] = useState<{ [key: number]: boolean }>({
        1: true,
        2: true,
        5: true,
        10: true
    });

    const toggleCoinEnable = (coinValue: number) => {
        setCoinsEnable((prevState) => {
            return {
                ...prevState,
                [coinValue]: !prevState[coinValue],
            };
        });
    };

    return (
        <div>
            <h1>Drink machine App</h1>
            <BrowserRouter>
                <div>
                
                    <nav className="navbar">
                        <ul className="navbar-list">
                            <li className="navbar-item">
                                <Link to="/admin" className="navbar-link">Admin</Link>
                            </li>
                            <li className="navbar-item">
                                <Link to="/user" className="navbar-link">User</Link>
                            </li>
                        </ul>
                    </nav>
            
                    <Routes>
                        <Route path="/admin" element={<AdminInterfacePage toggleCoinEnable={toggleCoinEnable} coinsEnable={coinsEnable} />} />
                        <Route path="/user" element={<UserInterfacePage coinsEnable={coinsEnable} />} />
                    </Routes>
                </div>
            </BrowserRouter>
        </div>
    );
}

export default App;