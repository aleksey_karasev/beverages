﻿

using Beverages.Application.DTOs.Coins;
using Beverages.Shared;

namespace Beverages.Application.Interfaces.Services
{
    public interface ICoinService
    {
        Task<Result<Guid>> Create(CoinDTOReqest request);
        Task<Result<bool>> Update(Guid Id, CoinDTOReqest request);
        Task<Result<bool>> Delete(Guid Id);

        Task<Result<CoinDTOResponse>> GetById(Guid Id);
        Task<Result<List<CoinDTOResponse>>> GetAll();

        Task<Result<uint>> GetCountCoin(Guid coinId);
        Task<Result<uint>> SetCountCoin(Guid id, uint count);
    }
}
