﻿using Beverages.Application.DTOs.Coins;
using Beverages.Application.Interfaces.Services;
using Beverages.Server.Filters;
using Beverages.Shared;
using Microsoft.AspNetCore.Mvc;

namespace Beverages.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CoinController : Controller
    {
        private readonly ICoinService _coinService;

        public CoinController(ICoinService coinService)
        {
            _coinService = coinService;
        }

        [SecretKeyFilter]
        [HttpPost("coins")]
        public async Task<ActionResult<Result<Guid>>> Create(CoinDTOReqest coinDTO)
        {
            var res = await _coinService.Create(coinDTO);
            return res.Success ? Ok(res) : BadRequest(res);
        }

        [SecretKeyFilter]
        [HttpPut("coins/{id}")]
        public async Task<Result<bool>> Update(Guid id, CoinDTOReqest coinDTO)
        {
            return await _coinService.Update(id, coinDTO);
        }

        [SecretKeyFilter]
        [HttpDelete("coins/{id}")]
        public async Task<Result<bool>> Delete(Guid id)
        {
            return await _coinService.Delete(id);
        }

        [SecretKeyFilter]
        [HttpGet("coins")]
        public async Task<Result<List<CoinDTOResponse>>> GetAllCoins()
        {
            return await _coinService.GetAll();
        }

        [SecretKeyFilter]
        [HttpGet("coins/{id}")]
        public async Task<Result<CoinDTOResponse>> GetCoinById(Guid id)
        {
            return await _coinService.GetById(id);
        }

        [SecretKeyFilter]
        [HttpGet("getCount/{id}")]
        public async Task<Result<uint>> GetCountCoin(Guid id)
        {
            return await _coinService.GetCountCoin(id);
        }

        [SecretKeyFilter]
        [HttpGet("setCount/{id}")]
        public async Task<Result<uint>> SetCountCoin(Guid id, uint count)
        {
            return await _coinService.SetCountCoin(id, count);
        }

    }
}
