﻿
using Beverages.Application.DTOs.Coins;
using Beverages.Shared;

namespace Beverages.Application.Interfaces.Services
{
    public interface IMoneyService
    {
        public Task<Result<decimal>> InsertMoney(CoinDTOReqest coin);

        public Task<Result<List<CoinDTOResponse>>> GetChange();

        public Task<Result<decimal>> ShowAmount();

        public Task<Result<decimal>> ToSpend(decimal money);
    }
}
