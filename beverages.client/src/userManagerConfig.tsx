import { UserManager, WebStorageStateStore } from 'oidc-client';

const config = {
    authority: 'https://localhost:5001',
    client_id: 'api',
    redirect_uri: window.location.origin,
    response_type: 'code',
    scope: 'openid profile BeveragesAPI',
     userStore: new WebStorageStateStore({ store: window.localStorage }),
};

const userManager = new UserManager(config);

export default userManager;
