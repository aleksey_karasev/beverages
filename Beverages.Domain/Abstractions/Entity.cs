﻿

using System.ComponentModel.DataAnnotations;

namespace Beverages.Domain.Abstractions
{
    public abstract class Entity : IEntity
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}
