﻿

namespace Beverages.Application.Services
{
    public interface IMoneyStateService
    {
        decimal Amount { get; set; }

        public void InsertAmount(decimal money);
        public void Clear();
    }
}
