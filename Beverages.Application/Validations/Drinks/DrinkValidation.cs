﻿

using Beverages.Application.DTOs.Drinks;
using FluentValidation;

namespace Beverages.Application.Validations.Drinks
{
    public class DrinkValidation : AbstractValidator<DrinkDTORequest>
    {
        public DrinkValidation()
        {
            RuleFor(x => x)
                .NotNull().WithMessage("Drink is null");

            RuleFor(x => x.Name)
                .NotEmpty().NotNull().WithMessage("Name is null")
                .MaximumLength(60).WithMessage("Max Length Name is 60");

            RuleFor(x => x.ImageName)
                .NotEmpty().NotNull().WithMessage("Photo is null");

            RuleFor(x => x.Price)
                .NotEmpty().NotNull().WithMessage("Price is null");
        }
    }
}
