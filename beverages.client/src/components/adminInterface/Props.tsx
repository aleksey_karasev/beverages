
export interface Drink {
    id: number | null;
    name: string;
    price: number | null;
    count: number | null;
    imageName: string;
}

export interface PropsModal {
    title: string;
    isOpen: boolean;
    disable: boolean;
    onClose: () => void;
    onSave: (drink: Drink) => void;
}

export default Drink;