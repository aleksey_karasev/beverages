﻿
using Microsoft.AspNetCore.Identity;

namespace Beverages.Identity.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
