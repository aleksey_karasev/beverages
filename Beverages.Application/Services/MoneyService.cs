﻿

using Beverages.Application.DTOs.Coins;
using Beverages.Application.Interfaces.Repositories;
using Beverages.Application.Interfaces.Services;
using Beverages.Domain.Entities;
using Beverages.Shared;
using Microsoft.EntityFrameworkCore;

namespace Beverages.Application.Services
{
    public class MoneyService : IMoneyService
    {

        private readonly IMoneyStateService _moneyStateService;

        private readonly IRepository<StorageCoin> _storageCoinRepository;
        private readonly IUnitOfWork _unitOfWork;

        public MoneyService(IUnitOfWork unitOfWork, IMoneyStateService moneyStateService)
        {
            _unitOfWork = unitOfWork;

            _storageCoinRepository = _unitOfWork.GetRepository<StorageCoin>();
            _moneyStateService = moneyStateService;
        }

        public async Task<Result<List<CoinDTOResponse>>> GetChange()
        {
            decimal change = _moneyStateService.Amount;
            

            var coins = new List<CoinDTOResponse>();

            change = await DistributeCoinsFromStorage(change, 10, coins);

            change = await DistributeCoinsFromStorage(change, 5, coins);

            change = await DistributeCoinsFromStorage(change, 2, coins);

            change = await DistributeCoinsFromStorage(change, 1, coins);

            _moneyStateService.Clear();

            return Result<List<CoinDTOResponse>>.Ok(coins, "Returns change");
        }

        private async Task<decimal> DistributeCoinsFromStorage(decimal change, decimal denomination, List<CoinDTOResponse> coins)
        {
            int count = (int)(change / denomination);
            if (count > 0)
            {
                var coin = await GetCoinFromStorage(denomination);

                if (coin is not null && coin.Count >= count)
                {
                    for (int i = 0; i < count; i++)
                    {
                        coin.Count--;
                        coins.Add(new CoinDTOResponse { Id = coin.Id, Denomination = denomination });
                        change -= denomination;
                    }

                    _storageCoinRepository.Update(coin);
                    await _unitOfWork.SaveChangesAsync();
                }
            }

            return change;
        }

        private async Task<StorageCoin> GetCoinFromStorage(decimal denomination)
        {
            var coinStorage = await _storageCoinRepository.Entities
                    .FirstOrDefaultAsync(f => f.Coin != null && f.Coin.Denomination == denomination);

            if (coinStorage is not null && coinStorage.Count > 0)
            {
                return coinStorage;
            }

            throw new Exception("Монеты данного номинала закончились в аппарате.");
        }


        public async Task<Result<decimal>> InsertMoney(CoinDTOReqest coin)
        {
            if (coin.Denomination <= 0)
                return Result<decimal>.Fail("Denomination is zero");

            var coinStorage = await GetCoinFromStorage(coin.Denomination ?? 0);

            if (coinStorage is not null)
            {
                _moneyStateService.InsertAmount(coin.Denomination ?? 0);

                coinStorage.Count++;

                _storageCoinRepository.Update(coinStorage);

                await _unitOfWork.SaveChangesAsync();

                return Result<decimal>.Ok(_moneyStateService.Amount, "Money inserted");
            }

            return Result<decimal>.Fail("Coin is not found");
        }

        public async Task<Result<decimal>> ShowAmount()
        {
            return Result<decimal>.Ok(_moneyStateService.Amount, "Show amount");
        }

        public async Task<Result<decimal>> ToSpend(decimal money)
        {
            if (_moneyStateService.Amount < money)
                return Result<decimal>.Fail("insufficient funds");

            _moneyStateService.Amount -= money;

            return Result<decimal>.Ok(_moneyStateService.Amount, "Money spent");
        }

    }
}
