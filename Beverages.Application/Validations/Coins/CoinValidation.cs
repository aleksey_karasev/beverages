﻿using Beverages.Application.DTOs.Coins;
using FluentValidation;

namespace Beverages.Application.Validations.Coins
{
    public class CoinValidation : AbstractValidator<CoinDTOReqest>
    {
        public CoinValidation()
        {
            RuleFor(x => x)
            .NotNull().WithMessage("Drink is null");

            RuleFor(x => x.Denomination)
                .NotEmpty().NotNull().WithMessage("Denomination is null");
        }
    }
}
