﻿
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Beverages.Application.DTOs.Drinks;
using Beverages.Application.Interfaces.Repositories;
using Beverages.Application.Interfaces.Services;
using Beverages.Application.Validations.Drinks;
using Beverages.Domain.Entities;
using Beverages.Shared;
using Microsoft.EntityFrameworkCore;

namespace Beverages.Application.Services
{
    public class DrinkService : IDrinkService
    {
        private readonly IMapper _mapper;

        private readonly IRepository<Drink> _DrinkRepository;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMoneyService _moneyService;

        DrinkValidation _validation;

        public DrinkService(IMapper mapper, IUnitOfWork unitOfWork,
            IMoneyService moneyService, DrinkValidation validation)
        {
            _mapper = mapper;
            _DrinkRepository = unitOfWork.GetRepository<Drink>();
            _unitOfWork = unitOfWork;

            _moneyService = moneyService;

            _validation = validation;
        }

        public async Task<Result<Guid>> Create(DrinkDTORequest request)
        {
            var valid = await _validation.ValidateAsync(request);

            if (valid.IsValid)
            {
                var drink = _mapper.Map<Drink>(request);

                drink.CreateDate = DateTime.UtcNow;

                await _DrinkRepository.AddAsync(drink);

                await _unitOfWork.SaveChangesAsync();

                var drinkDTO = _mapper.Map<DrinkDTOResponse>(drink);

                return Result<Guid>.Ok(drinkDTO.Id, "Create the drink");
            }

            return Result<Guid>.Fail(valid.ToString());
        }

        public async Task<Result<bool>> Delete(Guid Id)
        {
            var drink = await _DrinkRepository.GetAsync(Id);

            if (drink is not null)
            {
                _DrinkRepository.Delete(drink);

                await _unitOfWork.SaveChangesAsync();

                return Result<bool>.Ok(true, "Delete the drink");
            }

            return Result<bool>.Fail("Drink not found");
        }

        public async Task<Result<bool>> Update(Guid Id, DrinkDTORequest request)
        {
            var drink = await _DrinkRepository.GetAsync(Id);

            if (drink is not null)
            {
                if (!string.IsNullOrWhiteSpace(request.Name)) { drink.Name = request.Name; }
                if (!string.IsNullOrWhiteSpace(request.ImageName)) { drink.ImageName = request.ImageName; }
                if (request.Price is not null) { drink.Price = request.Price; }
                if (request.Count is not null) { drink.Count = request.Count; }

                drink.EditDate = DateTime.UtcNow;

                _DrinkRepository.Update(drink);

                await _unitOfWork.SaveChangesAsync();

                return Result<bool>.Ok(true, "Update the drink");
            }

            return Result<bool>.Fail($"Drink not found");
        }


        public async Task<Result<List<DrinkDTOResponse>>> GetAll()
        {
            var drinks = await _DrinkRepository.Entities
                                                    .ProjectTo<DrinkDTOResponse>(_mapper.ConfigurationProvider)
                                                    .ToListAsync();


            return Result<List<DrinkDTOResponse>>.Ok(drinks, "Get all drinks");
        }

        public async Task<Result<DrinkDTOResponse>> GetById(Guid Id)
        {
            var drink = await _DrinkRepository.GetAsync(Id);

            if (drink is not null)
            {
                var drinkDTO = _mapper.Map<DrinkDTOResponse>(drink);

                return Result<DrinkDTOResponse>.Ok(drinkDTO, "Get the drink");
            }

            return Result<DrinkDTOResponse>.Fail("Drink not found");
        }


        public async Task<Result<decimal>> Purchase(Guid drinkId)
        {
            var drink = await _DrinkRepository.GetAsync(drinkId);

            string error = "Drink not found";

            if (drink is not null)
            {
                error = "The drink is out";

                if (drink.Count > 0)
                {
                    var toSpend = await _moneyService.ToSpend(drink.Price ?? 0);

                    if (!toSpend.Success)
                        return Result<decimal>.Fail(toSpend.Message);

                    drink.Count--;

                    await _unitOfWork.SaveChangesAsync();

                    var amount = await _moneyService.ShowAmount();

                    return Result<decimal>.Ok(amount.Data, "Drink is purchase");
                }
            }

            return Result<decimal>.Fail(error);
        }

        public async Task<Result<uint>> GetCountDrink(Guid drinkId)
        {
            var drink = await _DrinkRepository.GetAsync(drinkId);

            if (drink is not null)
            {
                return Result<uint>.Ok(drink.Count ?? 0, "Get count the drink");
            }

            return Result<uint>.Fail("Drink not found");
        }

    }
}
