﻿
using Beverages.Application.Common.Mappings;
using Beverages.Domain.Entities;

namespace Beverages.Application.DTOs.StorageCoins
{
    public class StorageCoinDTOResponse : ICreateMap<StorageCoin>
    {
        public Guid Id { get; set; }

        public Guid CoinId { get; set; }
        public Coin? Coin { get; set; }
        public uint? Count { get; set; }
    }
}
