import React from 'react';

interface CoinsControlProps {
    coinsEnable: { [key: number]: boolean };
    toggleCoinEnable: (coinValue: number) => void;
}

const CoinsControl: React.FC<CoinsControlProps> = ({ coinsEnable, toggleCoinEnable }) => {

    const handleClick = (coinValue: number) => {
        toggleCoinEnable(coinValue);
    };

    return (
        <div>
            <h2>Coins Interface</h2>
            <br />
            <button onClick={() => handleClick(1)} >Toggle Coin 1 {coinsEnable[1] ? "(on)" : "(off)"}</button>
            <button onClick={() => handleClick(2)} >Toggle Coin 2 {coinsEnable[2] ? "(on)" : "(off)"}</button>
            <button onClick={() => handleClick(5)} >Toggle Coin 5 {coinsEnable[5] ? "(on)" : "(off)"}</button>
            <button onClick={() => handleClick(10)} >Toggle Coin 10 {coinsEnable[10] ? "(on)" : "(off)"}</button>
        </div>
    );
};

export default CoinsControl;