import { useEffect, useState } from 'react';
import './UserInterfacePage.css';

interface UserInterfaceProps {
    coinsEnable: { [key: number]: boolean };
}

const UserInterfacePage: React.FC<UserInterfaceProps> = ({ coinsEnable }) => {

    const [insertedAmount, setInsertedAmount] = useState<number>(0);
    const [beveragesData, setBeveragesData] = useState<any[]>([]);
    const [notification, setNotification] = useState<string>('');

    useEffect(() => {
        fetch('https://localhost:7168/Drink/drinks')
            .then(response => response.json())
            .then(data => setBeveragesData(data.data))
            .catch(error => console.error('Error fetching beverages data:', error));

        fetch('https://localhost:7168/Money/showAmount')
            .then(response => response.json())
            .then(data => setInsertedAmount(data.data))
            .catch(error => console.error('Error fetching beverages data:', error));

        setFadeIn(true);
    }, []);

    const handleReturnChange = (): void => {

        fetch(`https://localhost:7168/Money/getMoney`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
        .then(response => response.json())
        .then(data => {

            setNotification(`Your change: ${data.data.map((coin: { denomination: any; }) => coin.denomination).join(', ')}`);

            setTimeout(() => {

                setNotification('');
                window.location.reload();
            }, 3000);
        })
        .catch(error => console.error('Error fetching beverages data:', error));
        
    };

    const handleCoinClick = (denomination: number): void => {

        if (coinsEnable[denomination]) {
            
            fetch(`https://localhost:7168/Money/insertMoney`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ denomination: denomination.toString() })
            })
            .then(response => response.json())
            .then(data => {

                setInsertedAmount(data.data);

                setNotification('Amount successfully deposited');
            })
            .catch(error => console.error('Error fetching beverages data:', error));
        }
    };

    const handleBeverageClick = (beverageId: number, beveragePrice: number): void => {

        if (insertedAmount < beveragePrice) {
            setNotification('Insufficient funds');
        }

        if (beveragesData.find(beverage => beverage.id === beverageId)?.count <= 0) {
            setNotification('The product is out of stock');
        }

        fetch(`https://localhost:7168/Drink/drinks/purchase/${beverageId}`)
        .then(response => response.json())
        .then(data => {

            if (data.success) {
                setNotification('Product purchased successfully');

                setTimeout(() => {
                    window.location.reload();
                }, 3000);
            } 
        })
        .catch(error => console.error('Error fetching beverages data:', error));

    };

    const [fadeIn, setFadeIn] = useState(false);

    return (
        <div className={fadeIn ? "fadeIn" : ""}>

            <h2>User Interface</h2>

            <div style={{ border: '1px solid #ccc', borderRadius: '5px', padding: '10px', backgroundColor: '#f0f0f0' }}>
                <p style={{ margin: '0', fontSize: '18px', fontWeight: 'bold', color: '#333' }}>Inserted Amount: {insertedAmount} rub</p>
            </div>

            <br /> 
            <button onClick={handleReturnChange}>Return Change</button>
            <br /> 
                <div className="coin-container">
                    <div className={`coin ${coinsEnable[1] ? '' : 'disabled'}`} onClick={() => handleCoinClick(1)}>
                        <img src="../src/assets/coin.jpg" alt="1 ruble coin" />
                        <p>1 rub</p>
                    </div>
                    <div className={`coin ${coinsEnable[2] ? '' : 'disabled'}`} onClick={() => handleCoinClick(2)}>
                        <img src="../src/assets/coin.jpg" alt="2 rubles coin" />
                        <p>2 rub</p>
                    </div>
                    <div className={`coin ${coinsEnable[5] ? '' : 'disabled'}`} onClick={() => handleCoinClick(5)}>
                        <img src="../src/assets/coin.jpg" alt="5 rubles coin" />
                        <p>5 rub</p>
                    </div>
                    <div className={`coin ${coinsEnable[10] ? '' : 'disabled'}`} onClick={() => handleCoinClick(10)}>
                        <img src="../src/assets/coin.jpg" alt="10 rubles coin" />
                        <p>10 rub</p>
                    </div>
                </div>
            <br />
            <div className="beverage-container" style={{ display: 'flex' }}>
                {beveragesData.map((beverage, index) => (
                    <div key={beverage.id} className={`beverage ${beverage.quantity === 0 || beverage.price > insertedAmount ? 'disabled' : ''}`} style={{ marginRight: '10px', borderRight: index !== beveragesData.length - 1 ? '1px solid #ccc' : 'none' }}>
                        <img
                            src={`../src/assets/${beverage.imageName}`}
                            alt={beverage.name}
                            onClick={() => handleBeverageClick(beverage.id, beverage.price)}
                            style={{ maxWidth: '400px', maxHeight: '200px', width: 'auto', height: 'auto' }}
                        />
                        <p>{beverage.name}</p>
                        <p>Price: {beverage.price}</p>
                        <p>Count: {beverage.count}</p>
                    </div>
                ))}
            </div>
            
            {notification && (
                <p style={{ backgroundColor: 'lightblue', padding: '10px', borderRadius: '5px' }}>
                    {notification}
                </p>
            )}
        </div>
    );
}

export default UserInterfacePage;