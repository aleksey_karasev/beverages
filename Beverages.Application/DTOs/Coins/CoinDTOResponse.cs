﻿
using Beverages.Application.Common.Mappings;
using Beverages.Domain.Entities;

namespace Beverages.Application.DTOs.Coins
{
    public class CoinDTOResponse : ICreateMap<Coin>
    {
        public Guid Id { get; set; }
        public decimal? Denomination { get; set; }
    }
}
