﻿
using Beverages.Application.Common.Mappings;
using Beverages.Domain.Entities;

namespace Beverages.Application.DTOs.Coins
{
    public class CoinDTOReqest : ICreateMap<Coin>
    {
        public decimal? Denomination { get; set; }
    }
}
