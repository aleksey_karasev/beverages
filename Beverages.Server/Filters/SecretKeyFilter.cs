﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Beverages.Server.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
    public class SecretKeyFilter : ActionFilterAttribute
    {
        private const string SecretKeyKey = "secretKey";
        private const string SecretKeyValue = "secret_key";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // Check if secretKey is present in query parameters
            if (!context.HttpContext.Request.Query.ContainsKey(SecretKeyKey))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            // Get the value of secretKey from query parameters
            var secretKeyValue = context.HttpContext.Request.Query[SecretKeyKey];

            // Check secret key match
            if (secretKeyValue != SecretKeyValue)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
