﻿
using Beverages.Domain.Abstractions;

namespace Beverages.Domain.Entities
{
    public class StorageCoin : Entity
    {
        public Guid CoinId { get; set; }
        public virtual Coin? Coin { get; set; }
        public uint? Count { get; set; }
    }
}
