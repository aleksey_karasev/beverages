﻿using Duende.IdentityServer.Models;

namespace Beverages.Identity
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("BeveragesAPI"),
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "api",
                    ClientName = "Beverages",

                    AllowedGrantTypes = GrantTypes.Code,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    RedirectUris = { "https://localhost:5173;" },
                    PostLogoutRedirectUris = { "https://localhost:5173;" },
                    AllowedCorsOrigins = { "https://localhost:5173;" },

                    AllowedScopes = 
                    {
                        "openid profile",
                        "BeveragesAPI"
                    }
                },
            };
    }
}
