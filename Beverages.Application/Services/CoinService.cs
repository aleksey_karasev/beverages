﻿
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Beverages.Application.DTOs.Coins;
using Beverages.Application.Interfaces.Repositories;
using Beverages.Application.Interfaces.Services;
using Beverages.Application.Validations.Coins;
using Beverages.Domain.Entities;
using Beverages.Shared;
using Microsoft.EntityFrameworkCore;

namespace Beverages.Application.Services
{
    public class CoinService : ICoinService
    {
        private readonly IMapper _mapper;

        private readonly IRepository<Coin> _coinRepository;
        private readonly IRepository<StorageCoin> _storageCoinRepository;
        private readonly IUnitOfWork _unitOfWork;

        CoinValidation _validation;

        public CoinService(IMapper mapper, IUnitOfWork unitOfWork,
             CoinValidation validation)
        {
            _mapper = mapper;
            _coinRepository = unitOfWork.GetRepository<Coin>();
            _storageCoinRepository = unitOfWork.GetRepository<StorageCoin>();
            _unitOfWork = unitOfWork;

            _validation = validation;
        }


        public async Task<Result<Guid>> Create(CoinDTOReqest request)
        {
            var valid = await _validation.ValidateAsync(request);

            if (valid.IsValid)
            {
                var coin = _mapper.Map<Coin>(request);

                coin.CreateDate = DateTime.UtcNow;

                await _coinRepository.AddAsync(coin);

                await _storageCoinRepository.AddAsync(new StorageCoin
                {
                    CoinId = coin.Id,
                    Count = 0,
                    CreateDate = DateTime.UtcNow
                });

                await _unitOfWork.SaveChangesAsync();

                return Result<Guid>.Ok(coin.Id, "Create the coin");
            }

            return Result<Guid>.Fail(valid.ToString());
        }

        public async Task<Result<bool>> Delete(Guid Id)
        {
            var coin = await _coinRepository.GetAsync(Id);

            var storage = await _storageCoinRepository.Entities
                    .Include(c => c.CoinId)
                    .FirstOrDefaultAsync(f => f.CoinId == coin.Id);

            if (coin is not null || storage is not null)
            {
                _coinRepository.Delete(coin);

                _storageCoinRepository.Delete(storage);

                await _unitOfWork.SaveChangesAsync();

                return Result<bool>.Ok(true, "Delete the coin");
            }

            return Result<bool>.Fail("Coin is not found");
        }

        public async Task<Result<bool>> Update(Guid Id, CoinDTOReqest request)
        {
            var coin = await _coinRepository.GetAsync(Id);

            if (coin is not null)
            {
                if (request.Denomination is not null) { coin.Denomination = request.Denomination; }

                coin.EditDate = DateTime.UtcNow;

                _coinRepository.Update(coin);

                await _unitOfWork.SaveChangesAsync();

                return Result<bool>.Ok(true, "Update the coin");
            }

            return Result<bool>.Fail($"Coin is not found");
        }


        public async Task<Result<List<CoinDTOResponse>>> GetAll()
        {
            var coins = await _coinRepository.Entities
                                                    .ProjectTo<CoinDTOResponse>(_mapper.ConfigurationProvider)
                                                    .ToListAsync();

            return Result<List<CoinDTOResponse>>.Ok(coins, "Get all coins");
        }

        public async Task<Result<CoinDTOResponse>> GetById(Guid Id)
        {
            var coin = await _coinRepository.GetAsync(Id);

            if (coin is not null)
            {
                var coinDTO = _mapper.Map<CoinDTOResponse>(coin);

                return Result<CoinDTOResponse>.Ok(coinDTO, "Get the coin");
            }

            return Result<CoinDTOResponse>.Fail("Coin is not found");
        }


        public async Task<Result<uint>> GetCountCoin(Guid coinId)
        {
            var storageCoin = await _storageCoinRepository.Entities.FirstOrDefaultAsync(f => f.CoinId == coinId);

            if (storageCoin is not null)
            {
                return Result<uint>.Ok(storageCoin.Count ?? 0, "Get count the coin");
            }

            return Result<uint>.Fail("Coin is not found");
        }

        public async Task<Result<uint>> SetCountCoin(Guid coinId, uint count)
        {
            var storageCoin = await _storageCoinRepository.Entities.FirstOrDefaultAsync(f => f.CoinId == coinId);

            if (storageCoin is not null)
            {
                storageCoin.Count = count;

                _storageCoinRepository.Update(storageCoin);

                await _unitOfWork.SaveChangesAsync();

                return Result<uint>.Ok(storageCoin.Count ?? 0, "Get count the coin");
            }

            return Result<uint>.Fail("Coin is not found");
        }

    }
}
