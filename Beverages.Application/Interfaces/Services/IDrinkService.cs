﻿

using Beverages.Application.DTOs.Drinks;
using Beverages.Shared;

namespace Beverages.Application.Interfaces.Services
{
    public interface IDrinkService
    {
        Task<Result<Guid>> Create(DrinkDTORequest request);
        Task<Result<bool>> Update(Guid Id, DrinkDTORequest request);
        Task<Result<bool>> Delete(Guid Id);

        Task<Result<DrinkDTOResponse>> GetById(Guid Id);
        Task<Result<List<DrinkDTOResponse>>> GetAll();

        Task<Result<decimal>> Purchase(Guid drinkId);

        Task<Result<uint>> GetCountDrink(Guid drinkId);
    }
}
