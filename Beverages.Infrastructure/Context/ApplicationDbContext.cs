﻿
using Beverages.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Beverages.Persistence.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Coin> Coins { get; set; }
        public DbSet<StorageCoin> StorageCoins { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            #region "Validation in Database"

            modelBuilder.Entity<Drink>()
                .Property(e => e.Id).IsRequired();

            modelBuilder.Entity<Drink>()
                .Property(e => e.Name).HasMaxLength(60);

            modelBuilder.Entity<Drink>()
                .Property(e => e.ImageName).HasMaxLength(200);

            #endregion

            base.OnModelCreating(modelBuilder);

        }

    }
}
