import React, { useState } from 'react';
import Drink, { PropsModal } from './Props';
import './AdminInterfacePage.css';

const DrinkModal: React.FC<PropsModal> = ({ title, isOpen, disable, onClose, onSave }) => {

    const [formData, setFormData] = useState<Drink>({
        id: 0,
        name: '',
        price: null,
        count: null,
        imageName: ''
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData(prevData => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        onSave(formData);
        onClose();
    };

    return (
        <div className={`modal ${isOpen ? 'is-open' : ''}`}>
            <div className="modal-content">
                <h2>{title}</h2>
                <form onSubmit={handleSubmit} className="custom-form">
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" id="name" name="name" value={formData.name} onChange={handleChange} disabled={disable} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="price">Price:</label>
                        <input type="number" id="price" name="price" value={formData.price?.toString()} onChange={handleChange} disabled={disable} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="count">Count:</label>
                        <input type="number" id="count" name="count" value={formData.count?.toString()} onChange={handleChange} disabled={disable} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="imageName">Image:</label>
                        <input type="text" id="imageName" name="imageName" value={formData.imageName} onChange={handleChange} disabled={disable} />
                    </div>
                    <div className="button-group">
                        <button type="submit">{title}</button>
                        <button type="button" onClick={onClose}>Close</button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default DrinkModal;