﻿

using Beverages.Domain.Abstractions;

namespace Beverages.Domain.Entities
{
    public class Drink : Entity
    {
        public string? Name { get; set; }
        public string? ImageName { get; set; }
        public decimal? Price { get; set; }
        public uint? Count { get; set; }
    }
}
