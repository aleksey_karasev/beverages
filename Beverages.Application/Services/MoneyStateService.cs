﻿
namespace Beverages.Application.Services
{
    public class MoneyStateService : IMoneyStateService
    {
        public decimal Amount { get; set; }

        public MoneyStateService()
        {
            Clear();
        }

        public void InsertAmount(decimal money) => Amount += money;
        public void Clear() => Amount = 0;
    }
}
